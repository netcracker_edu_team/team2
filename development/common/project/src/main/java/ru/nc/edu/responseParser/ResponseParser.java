package ru.nc.edu.responseParser;

interface ResponseParser {
    Object getElementByName(String input, String element);
    int getCountOfElements(String input, String element);
}
