package ru.nc.edu.database;

import org.apache.log4j.Logger;
import ru.nc.edu.utils.ApplicationContext;
import ru.nc.edu.entities.PowerEntity;
import ru.nc.edu.entities.WeatherEntity;
import ru.nc.edu.entities.WindTurbineEntity;
import ru.nc.edu.weatherParser.WeatherParserCreator;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public class DatabaseConnectionImpl implements DatabaseConnection{
    private Connection connection;
    private static final Logger LOG=Logger.getLogger(DatabaseConnectionImpl.class);

    public DatabaseConnectionImpl() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            if (LOG.isInfoEnabled())
                LOG.info("Driver successfully register");

            getConnection();
            closeConnection();
        } catch (SQLException ex) {
            LOG.error("Could not connect to database", ex);
        }
    }

    public Connection getConnection() {
        try {
            connection = DriverManager.getConnection(
                    ApplicationContext.getPropertyByName("DB_URL"),
                    ApplicationContext.getPropertyByName("DB_USERNAME"),
                    ApplicationContext.getPropertyByName("DB_PASSWORD"));
            if (connection.isClosed()) {
                throw new SQLException();
            }
            if (LOG.isInfoEnabled())
                LOG.info("Database connect successfully");
        } catch (SQLException ex) {
            LOG.error("Could not connect to database", ex);
        } finally {
            return connection;
        }
    }

    public void closeConnection() {
        try {
            connection.close();
            if (LOG.isInfoEnabled())
                LOG.info("Database connection close successfully");
        } catch (SQLException ex) {
            LOG.error("Could not close connection to database", ex);
        }
    }

    public void insertForecast(WeatherParserCreator responseParseApiCreator) {
        try {
            getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement
                    = connection.prepareStatement(DatabaseConnectionStatements.INSERT_FORECAST);

            Set<Long> times = responseParseApiCreator.getTimes();
            for (Long time: times) {
                WeatherEntity weather = responseParseApiCreator.getWheatherByTime(time);
                preparedStatement.setLong(1, time);
                preparedStatement.setDouble(2, weather.getSpeed());
                preparedStatement.setDouble(3, weather.getTemperature());
                preparedStatement.setDouble(4, weather.getPressure());
                preparedStatement.setDouble(5, weather.getHumidity());
                preparedStatement.setDouble(6, weather.getSpeed());
                preparedStatement.setDouble(7, weather.getTemperature());
                preparedStatement.setDouble(8, weather.getPressure());
                preparedStatement.setDouble(9, weather.getHumidity());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            connection.commit();

            if (LOG.isInfoEnabled())
                LOG.info("Forecast was inserted to database");

        } catch (SQLException ex) {
            LOG.error("Enable execute statement", ex);
            try {
                connection.rollback();
            } catch (SQLException e) {
                LOG.error("Enable rollback transaction", ex);
            }
        } finally {
            closeConnection();
        }
    }

    public void truncateForecast() {
        try {
            getConnection();
            Statement statement = connection.createStatement();
            statement.execute("TRUNCATE TABLE forecast;");

            if (LOG.isInfoEnabled())
                LOG.info("Forecast table was truncated");
        } catch (SQLException ex) {
            LOG.error("Enable execute statement", ex);
        } finally {
            closeConnection();
        }
    }

    public WindTurbineEntity getWindTurbineById(int id) {
        try {
            getConnection();
            String selectSql = DatabaseConnectionStatements.SELECT_TURBINE_BY_ID;
            PreparedStatement preparedStatement
                    = connection.prepareStatement(selectSql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return new WindTurbineEntity(resultSet.getInt("id"), resultSet.getString("producer"),
                        resultSet.getDouble("longtitude"), resultSet.getInt("latitude"),
                        resultSet.getDouble("cutIn"), resultSet.getDouble("cutOut"),
                        resultSet.getDouble("cutNom"), resultSet.getInt("powerNom"));
            }
            if (LOG.isInfoEnabled())
                LOG.info("Turbine with id = " + id + "was selected");
        } catch (SQLException ex) {
            LOG.error("Enable execute statement", ex);
        } finally {
            closeConnection();
        }
        return null;
    }

    public Collection<WindTurbineEntity> getWindTurbines() {
        try {
            getConnection();
            String selectSql = DatabaseConnectionStatements.SELECT_TURBINES;
            Statement statement
                    = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectSql);

            Collection<WindTurbineEntity> result = new ArrayList<WindTurbineEntity>();

            while (resultSet.next()) {
                result.add(new WindTurbineEntity(resultSet.getInt("id"), resultSet.getString("producer"),
                        resultSet.getDouble("longtitude"),resultSet.getInt("latitude"),
                        resultSet.getDouble("cutIn"), resultSet.getDouble("cutOut"),
                        resultSet.getDouble("cutNom"),resultSet.getInt("powerNom")));
            }

            if (LOG.isInfoEnabled())
                LOG.info("Turbines was selected");

            return result;
        } catch (SQLException ex) {
            LOG.error("Enable execute statement", ex);
        } finally {
            closeConnection();
        }
        return null;
    }

    public Collection<PowerEntity> getConsumersConsumption() {
        try {
            getConnection();
            String selectSql = DatabaseConnectionStatements.SELECT_CONSUMERS_CONSUMPTION;
            Statement statement
                    = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectSql);

            Collection<PowerEntity> result = new ArrayList<PowerEntity>();

            while (resultSet.next()) {
                PowerEntity powerEntity = new PowerEntity();
                powerEntity.setDate(resultSet.getTime("time"));
                powerEntity.setPower((int)resultSet.getDouble("power"));
                result.add(powerEntity);
            }
            if (LOG.isInfoEnabled())
                LOG.info("Consumers consumption was selected");
            return result;

        } catch (SQLException ex) {
            LOG.error("Enable execute statement", ex);
        } finally {
            closeConnection();
        }
        return null;
    }

    public Collection<WeatherEntity> getWeather() {
        try {
            getConnection();
            String selectSql = DatabaseConnectionStatements.SELECT_FORECAST;
            Statement statement
                    = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectSql);

            Collection<WeatherEntity> result = new ArrayList<WeatherEntity>();

            while (resultSet.next()) {
                WeatherEntity weatherCondition = new WeatherEntity();
                weatherCondition.setDate(resultSet.getTimestamp("time"));
                weatherCondition.setSpeed(String.valueOf(resultSet.getDouble("windSpeed")));
                weatherCondition.setHumidity(String.valueOf(resultSet.getDouble("humidity")));
                weatherCondition.setTemperature(String.valueOf(resultSet.getDouble("temperature")));
                weatherCondition.setPressure(String.valueOf(resultSet.getDouble("pressure")));
                result.add(weatherCondition);
            }
            if (LOG.isInfoEnabled())
                LOG.info("Forecast was selected");
            return result;

        } catch (SQLException ex) {
            LOG.error("Enable execute statement", ex);
        } finally {
            closeConnection();
        }
        return null;
    }

}
