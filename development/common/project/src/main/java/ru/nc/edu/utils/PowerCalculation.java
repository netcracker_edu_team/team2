package ru.nc.edu.utils;

import org.apache.commons.math3.analysis.UnivariateFunction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ru.nc.edu.entities.PowerEntity;
import ru.nc.edu.entities.WeatherEntity;
import ru.nc.edu.entities.WindTurbineEntity;

public class PowerCalculation {

    private final Double cutIn;
    private final Double cutOut;
    private final Double cutNom;
    private final Integer powerNom;
   
    public PowerCalculation(WindTurbineEntity windTurbineEntity) {
        this.cutIn = windTurbineEntity.getCutIn();
        this.cutOut = windTurbineEntity.getCutOut();
        this.cutNom = windTurbineEntity.getCutNom();
        this.powerNom = windTurbineEntity.getPowerNom();
    }

    public List<PowerEntity> estimatePower(Collection<WeatherEntity> wl) {
        List<PowerEntity> list = new ArrayList<PowerEntity>();
        for (WeatherEntity w : wl) {
            double wind = w.getSpeed()+6.5;
            PowerEntity p = new PowerEntity();
            if (wind >= cutNom && wind < cutOut) {
                p.setPower(powerNom );
                p.setTemperature(w.getTemperature());
                p.setPressure(w.getPressure() /10);
            } else if (wind  >= cutOut) {
                p.setPower(0);
            } else if (wind < cutIn) {
                p.setPower(0);
            } else if (wind >= cutIn &&wind < cutNom) {

                Double airDensity = (w.getPressure() * 29)/(10*8.3144598*(273.15+w.getTemperature()));
                Double powerAfter = 0.5 * airDensity * Math.PI * calculatePowerCoefficient (wind) * Math.pow(41, 2) * Math.pow((wind), 3) * 0.9;

                if(powerAfter >= powerNom){
                    p.setPower(powerNom );
                }
                else {
                    p.setPower(powerAfter.intValue() );
                }

                p.setTemperature(w.getTemperature());
                p.setPressure(w.getPressure() /10);
            }
            p.setDate(w.getDate());
            p.setWind(wind);
            list.add(p);

        }

        return list;
    }

    private double calculatePowerCoefficient(double w) {

        double x[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
        double y[] = {0, 0.19, 0.39, 0.42, 0.45, 0.48, 0.47, 0.46, 0.43, 0.38, 0.35, 0.30, 0.25, 0.22, 0.18, 0.15, 0.12, 0.1, 0.009, 0.08, 0.06, 0.06, 0.05, 0.04, 0.04};

        UnivariateFunction f = Mathematic.interpolate(y, x);
        return f.value(w);
    }
}
