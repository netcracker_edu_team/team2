package ru.nc.edu.utils;

import org.apache.log4j.Logger;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Екатерина on 20.03.2016.
 */
public class DateAdapter extends XmlAdapter<String, Date> {
    private static final Logger LOG=Logger.getLogger(DateAdapter.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss");

    @Override
    public String marshal(Date v) {
        return dateFormat.format(v);
    }

    @Override
    public Date unmarshal(String v) {
        try {
            return dateFormat.parse(v);
        } catch (ParseException e) {
            LOG.error("Error parsing date");
        }
        return null;
    }
}
