package ru.nc.edu.weatherParser;

import ru.nc.edu.entities.WeatherEntity;

import java.util.Set;

interface WeatherParser {
    WeatherEntity getWheatherByTime(Long time);
    Set<Long> getTimes();
}
