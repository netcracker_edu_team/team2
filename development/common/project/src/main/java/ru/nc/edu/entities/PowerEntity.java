package ru.nc.edu.entities;

import ru.nc.edu.utils.DateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class PowerEntity  implements BaseEntity{
    @XmlElement
    private Integer power;
    @XmlElement
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date date;
    @XmlElement
    private Long id;
    @XmlElement
    private Integer version;
    @XmlElement
    private Double wind;
    @XmlElement
    private Double pressure;
    @XmlElement
    private Double temperature;

    public PowerEntity() {}

    public void setWind(Double wind) {
        this.wind = wind;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "PowerEntity{" +
                "power=" + power +
                ", date=" + date +
                ", id=" + id +
                ", version=" + version +
                ", wind=" + wind +
                ", pressure=" + pressure +
                ", temperature=" + temperature +
                '}';
    }
}
