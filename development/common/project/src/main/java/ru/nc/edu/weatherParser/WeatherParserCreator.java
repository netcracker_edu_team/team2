package ru.nc.edu.weatherParser;

import org.apache.log4j.Logger;
import ru.nc.edu.entities.WeatherEntity;

import java.util.Set;

/**
 * Created by Екатерина on 12.03.2016.
 */
public class WeatherParserCreator implements WeatherParser {
    private static WeatherParser responseParseApi;
    private static final Logger LOG=Logger.getLogger(WeatherParserCreator.class);

    public WeatherParserCreator(String responseParserApiName, String response) {
        if (responseParserApiName.equals("xml")) {
            responseParseApi = new WeatherParserXML(response);
            if (LOG.isInfoEnabled())
                LOG.info("Create WeatherParserCreator for XML");
        } else if (responseParserApiName.equals("json")) {
            responseParseApi = new WeatherParserJSON(response);
            if (LOG.isInfoEnabled())
                LOG.info("Create WeatherParserCreator for JSON");
        }
    }

    public WeatherEntity getWheatherByTime(Long time) {
        return responseParseApi.getWheatherByTime(time);
    }

    public Set<Long> getTimes() {
        return responseParseApi.getTimes();
    }
}
