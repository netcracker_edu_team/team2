package ru.nc.edu.database;

import ru.nc.edu.entities.PowerEntity;
import ru.nc.edu.entities.WeatherEntity;
import ru.nc.edu.entities.WindTurbineEntity;
import ru.nc.edu.weatherParser.WeatherParserCreator;

import java.sql.Connection;
import java.util.Collection;

interface DatabaseConnection {
    Connection getConnection();
    void closeConnection();
    void insertForecast(WeatherParserCreator responseParseApiCreator);
    void truncateForecast();

    WindTurbineEntity getWindTurbineById(int id);
    Collection<WindTurbineEntity> getWindTurbines();
    Collection<PowerEntity> getConsumersConsumption();
    Collection<WeatherEntity> getWeather();
}
