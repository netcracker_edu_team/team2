package ru.nc.edu.database;

import ru.nc.edu.utils.ApplicationContext;
import ru.nc.edu.entities.PowerEntity;
import ru.nc.edu.entities.WeatherEntity;
import ru.nc.edu.entities.WindTurbineEntity;
import ru.nc.edu.weatherParser.WeatherParserCreator;
import ru.nc.edu.URLConnection.HttpUrlConnectionCreator;

import java.util.Collection;

public class DatabaseWriter {
    private final DatabaseConnection databaseConnection = new DatabaseConnectionImpl();

    public void writeToDatabase() {
        HttpUrlConnectionCreator httpConnection=
                new HttpUrlConnectionCreator(HttpUrlConnectionCreator.HttpUrlConnectionName.APACHE_HTTP_CLIENT);
        String response = httpConnection.sendGet();
        WeatherParserCreator responseParseApiCreator = new WeatherParserCreator(ApplicationContext.getPropertyByName("mode"),
                response);
        databaseConnection.truncateForecast();
        databaseConnection.insertForecast(responseParseApiCreator);
    }

    public WindTurbineEntity getWindTurbineById(int id) {
        return databaseConnection.getWindTurbineById(id);
    }

    public Collection<WindTurbineEntity> getWindTurbines() {
        return databaseConnection.getWindTurbines();
    }

    public Collection<PowerEntity> getConsumersConsumption() {
        return databaseConnection.getConsumersConsumption();
    }

    public Collection<WeatherEntity> getForecast() {
        return databaseConnection.getWeather();
    }
}
