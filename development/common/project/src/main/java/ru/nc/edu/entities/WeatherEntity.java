package ru.nc.edu.entities;

import ru.nc.edu.utils.DateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class WeatherEntity implements BaseEntity{
    @XmlElement
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date date;
    @XmlElement
    private double speed;
    @XmlElement
    private double humidity;
    @XmlElement
    private double pressure;
    @XmlElement
    private double temperature;

    public void setSpeed(String speed) {
        this.speed = Double.parseDouble(speed);
    }

    public void setHumidity(String humidity) {
        this.humidity = Double.parseDouble(humidity);
    }

    public void setPressure(String pressure) {
        this.pressure = Double.parseDouble(pressure);
    }

    public void setTemperature(String temperature) {
        this.temperature = Double.parseDouble(temperature);
    }

    public void setDate(Date date) {this.date = date; }

    public Date getDate() {
        return date;
    }

    public double getSpeed() {
        return this.speed;
    }

    public double getHumidity() {
        return this.humidity;
    }

    public double getPressure() {
        return this.pressure;
    }

    public double getTemperature() {
        return this.temperature;
    }
}
