package ru.nc.edu.weatherParser;

import org.apache.log4j.Logger;
import ru.nc.edu.entities.WeatherEntity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 * Created by Екатерина on 12.03.2016.
 */
class WeatherParserXML extends WeatherParserAbstract implements WeatherParser {

    private static final Logger LOG=Logger.getLogger(WeatherParserXML.class);
    public WeatherParserXML(String response) {
        super("xml", response);
        try {
            Calendar calendar = Calendar.getInstance();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date;

            int length = parser.getCountOfElements(response, "time");
            for (Integer i = 1; i <= length; i++) {
                String request = "weatherdata/forecast/time[" + i.toString() + "]/";

                String time = parser.getElementByName(response, request + "@from").toString();
                date = dateFormat.parse(time);
                calendar.setTime(date);

                WeatherEntity wheather = new WeatherEntity();
                wheather.setDate(date);
                wheather.setSpeed(parser.getElementByName(response, request + "windSpeed/@mps").toString());
                wheather.setHumidity(parser.getElementByName(response, request + "humidity/@value").toString());
                wheather.setPressure(parser.getElementByName(response, request + "pressure/@value").toString());
                wheather.setTemperature(parser.getElementByName(response, request + "temperature/@value").toString());

                properties.put(date.getTime(), wheather);
            }
        } catch (ParseException ex) {
            LOG.error("Unable to parse date :", ex);
        }
    }

    public WeatherEntity getWheatherByTime(Long time) {
        return properties.get(time);
    }

    public Set<Long> getTimes() {
        return properties.keySet();
    }
}
