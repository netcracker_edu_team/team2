package ru.nc.edu.weatherParser;

import ru.nc.edu.entities.WeatherEntity;
import ru.nc.edu.responseParser.ResponseParserCreator;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Екатерина on 11.03.2016.
 */
abstract class WeatherParserAbstract {

    protected final Map<Long, WeatherEntity> properties = new LinkedHashMap<Long, WeatherEntity>();
    protected ResponseParserCreator parser;

    protected WeatherParserAbstract() { }

    protected WeatherParserAbstract(String mode, String response) {
        parser = new ResponseParserCreator(mode);
    }

}
