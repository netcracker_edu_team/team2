package ru.nc.edu.responseParser;

import org.apache.log4j.Logger;

public class ResponseParserCreator implements ResponseParser{
    private static ResponseParser responseParser;
    private static final Logger LOG=Logger.getLogger(ResponseParserCreator.class);

    public ResponseParserCreator(String responseParserName) {
        if (responseParserName.equals("xml")) {
            responseParser = new XMLResponseParser();
            if (LOG.isInfoEnabled())
                LOG.info("Create ResponseParser for XML");
        } else if (responseParserName.equals("json")) {
            responseParser = new JSONResponseParser();
            if (LOG.isInfoEnabled())
                LOG.info("Create ResponseParser for JSON");
        }
    }

    public Object getElementByName(String input, String element) {
        return responseParser.getElementByName(input, element);
    }

    public int getCountOfElements(String input, String element) {
        return responseParser.getCountOfElements(input, element);
    }
}
