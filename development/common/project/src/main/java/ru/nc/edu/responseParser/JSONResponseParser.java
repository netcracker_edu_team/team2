package ru.nc.edu.responseParser;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

public class JSONResponseParser implements ResponseParser {
    private static final Logger LOG=Logger.getLogger(JSONResponseParser.class);

    private Object getElementByPartName(String input, String element) {
        try{
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(input);
            return obj.get(element);
        } catch(ParseException e){
            LOG.error("Error parse JSON by name:", e);
        }
        return null;
    }

    private boolean isNumber(String element) {
        try {
            Integer i = Integer.valueOf(element);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public Object getElementByName(String input, String element) {
        String[] elements = element.split("/");
        String tmpJson = input;

        for (String str: elements ) {
            if (isNumber(str)) {
                tmpJson = getElementById(tmpJson, Integer.valueOf(str)).toString();
            } else {
                tmpJson = getElementByPartName(tmpJson, str).toString();
            }
        }
        return tmpJson;
    }

    public int getCountOfElements(String input, String element) {
        org.codehaus.jettison.json.JSONObject jsonObject = null;
        try {
            jsonObject = new org.codehaus.jettison.json.JSONObject(input);
        } catch (JSONException e) {
            LOG.error("Error parse JSON");
        }
        org.codehaus.jettison.json.JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray(element);
        } catch (JSONException e) {
            LOG.error("Error parse JSON");
        }
        return jsonArray.length();
    }

    private Object getElementById(String input, Integer element) {
        try{
            JSONParser parser = new JSONParser();
            JSONArray obj = (JSONArray) parser.parse(input);
            return obj.get(element);
        } catch(ParseException e){
            LOG.error("Error parse JSON by index:", e);
        }
        return null;
    }
}
