package ru.nc.edu.server;

import ru.nc.edu.entities.PowerEntity;
import ru.nc.edu.entities.WeatherEntity;
import ru.nc.edu.entities.WindTurbineEntity;
import ru.nc.edu.database.DatabaseWriter;
import ru.nc.edu.utils.PowerCalculation;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Collection;

/**
 * Created by Екатерина on 17.03.2016.
 */
@Path("/app")
public class ServerMain {

    @GET
    @Path("/{param}")
    public Response getMsg(@PathParam("param") String msg) {

        String output = "Jersey say : " + msg;

        return Response.status(200).entity(output).build();

    }

    @GET
    @Path("/getConsumersConsumption")
    @Produces("application/json")
    public Collection<PowerEntity> getConsumersConsumption() {
        try {
            DatabaseWriter databaseWriter = new DatabaseWriter();
            return databaseWriter.getConsumersConsumption();
        } catch (Exception e) {
            throw new WebApplicationException(404);
        }
    }

    @GET
    @Path("/getWindPowerPredictionById")
    @Produces("application/json")
    public Collection<PowerEntity> getWindPowerPredictionById(@QueryParam("id") int id) {
        try {
            DatabaseWriter databaseWriter = new DatabaseWriter();
            databaseWriter.writeToDatabase();
            PowerCalculation powerCalculation
                    = new PowerCalculation(databaseWriter.getWindTurbineById(id));

            return powerCalculation.estimatePower(databaseWriter.getForecast());
        } catch (Exception e) {
            throw new WebApplicationException(404);
        }
    }

    @GET
    @Path("/getWindPowerPrediction")
    @Produces("application/json")
    public Collection<PowerEntity> getWindPowerPrediction() {
        try {
            DatabaseWriter databaseWriter = new DatabaseWriter();
            databaseWriter.writeToDatabase();
            Collection<WindTurbineEntity> windTurbineEntities
                    = databaseWriter.getWindTurbines();

            Collection<WeatherEntity> forecast = databaseWriter.getForecast();

            PowerCalculation powerCalculation
                    = new PowerCalculation((WindTurbineEntity)(windTurbineEntities.toArray())[0]);

            Collection<PowerEntity> result = powerCalculation.estimatePower(forecast);
            for (int i = 1; i < windTurbineEntities.size(); i++) {
                powerCalculation = new PowerCalculation((WindTurbineEntity)(windTurbineEntities.toArray())[i]);
                Collection<PowerEntity> tmpResult = powerCalculation.estimatePower(forecast);
                for (int j = 0; j < tmpResult.size(); j++) {
                    ((PowerEntity)(result.toArray()[j])).setPower(
                            ((PowerEntity)(result.toArray()[j])).getPower() +
                                    ((PowerEntity)(tmpResult.toArray()[j])).getPower());
                }
            }

            return result;
        } catch (Exception e) {
            throw new WebApplicationException(404);
        }
    }

    @GET
    @Path("/getTurbine")
    @Produces("application/json")
    public WindTurbineEntity getWindTurbineById(@QueryParam("id") int id) {
        try {
            DatabaseWriter databaseWriter = new DatabaseWriter();
            return databaseWriter.getWindTurbineById(id);
        } catch (Exception e) {
            throw new WebApplicationException(404);
        }
    }

    @GET
    @Path("/getTurbines")
    @Produces("application/json")
    public Collection<WindTurbineEntity> getWindTurbines() {
        try {
            DatabaseWriter databaseWriter = new DatabaseWriter();
            return databaseWriter.getWindTurbines();
        } catch (Exception e) {
            throw new WebApplicationException(404);
        }
    }

}
