package ru.nc.edu.weatherParser;

import org.apache.log4j.Logger;
import ru.nc.edu.entities.WeatherEntity;

import java.util.Date;
import java.util.Set;

/**
 * Created by Екатерина on 11.03.2016.
 */
class WeatherParserJSON extends WeatherParserAbstract implements WeatherParser {
    private static final Logger LOG=Logger.getLogger(WeatherParserJSON.class);

    public WeatherParserJSON(String response) {
        super("json", response);
        try {
            int length = parser.getCountOfElements(response, "list");

            for (Integer i = 0; i < length; i++) {
                String request = "list/" + i.toString() + "/";

                String time = parser.getElementByName(response, request + "dt").toString();
                Date date = new Date(Long.parseLong(time) *1000);

                WeatherEntity wheather = new WeatherEntity();
                wheather.setDate(date);
                wheather.setSpeed(parser.getElementByName(response, request + "wind/speed").toString());
                wheather.setHumidity(parser.getElementByName(response, request + "main/humidity").toString());
                wheather.setPressure(parser.getElementByName(response, request + "main/pressure").toString());
                wheather.setTemperature(parser.getElementByName(response, request + "main/temp").toString());

                properties.put(Long.parseLong(time)*1000, wheather);
            }
        } catch (Exception ex) {
            LOG.error("Unable to parse date :", ex);
        }
    }


    public WeatherEntity getWheatherByTime(Long time) {
        return properties.get(time);
    }

    public Set<Long> getTimes() {
        return properties.keySet();
    }
}
