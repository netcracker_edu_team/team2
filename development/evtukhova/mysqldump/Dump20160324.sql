CREATE DATABASE  IF NOT EXISTS `dbnc` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dbnc`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: dbnc
-- ------------------------------------------------------
-- Server version	5.5.48

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `consumersconsumption`
--

DROP TABLE IF EXISTS `consumersconsumption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumersconsumption` (
  `time` time NOT NULL,
  `power` double unsigned DEFAULT NULL,
  PRIMARY KEY (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumersconsumption`
--

LOCK TABLES `consumersconsumption` WRITE;
/*!40000 ALTER TABLE `consumersconsumption` DISABLE KEYS */;
INSERT INTO `consumersconsumption` VALUES ('00:00:00',500),('02:00:00',500),('04:00:00',500),('06:00:00',500),('08:00:00',800),('10:00:00',1200),('12:00:00',1300),('14:00:00',1250),('16:00:00',1500),('18:00:00',1600),('20:00:00',900),('22:00:00',700);
/*!40000 ALTER TABLE `consumersconsumption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forecast`
--

DROP TABLE IF EXISTS `forecast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forecast` (
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `windSpeed` double NOT NULL,
  `temperature` double NOT NULL,
  `pressure` double NOT NULL,
  `humidity` double NOT NULL,
  PRIMARY KEY (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forecast`
--

LOCK TABLES `forecast` WRITE;
/*!40000 ALTER TABLE `forecast` DISABLE KEYS */;
INSERT INTO `forecast` VALUES ('2016-03-22 06:00:00',2.16,1.1,994.8,96),('2016-03-22 09:00:00',1.76,0.8,995.15,94),('2016-03-22 12:00:00',2.81,-0.73,995.61,94),('2016-03-22 15:00:00',3.31,-1.27,995.74,93),('2016-03-22 18:00:00',4.15,-1.73,996,92),('2016-03-22 21:00:00',5.21,-2.37,996.33,88),('2016-03-23 00:00:00',5.81,-2.64,997.37,92),('2016-03-23 03:00:00',5.66,-1.89,998.51,93),('2016-03-23 06:00:00',5.36,-1.36,999.71,92),('2016-03-23 09:00:00',4.96,-1.92,1001.54,91),('2016-03-23 12:00:00',4.57,-2.81,1003.34,86),('2016-03-23 15:00:00',3.92,-3.42,1004.46,89),('2016-03-23 18:00:00',3.61,-4.39,1005.25,90),('2016-03-23 21:00:00',3.51,-5.56,1006.14,89),('2016-03-24 00:00:00',3.47,-5.78,1006.83,90),('2016-03-24 03:00:00',3.47,-3.92,1007.29,95),('2016-03-24 06:00:00',3.67,-2.75,1007.04,93),('2016-03-24 09:00:00',3.91,-3.58,1007.38,90),('2016-03-24 12:00:00',4.11,-5.05,1008.46,88),('2016-03-24 15:00:00',4.21,-6.01,1008.63,87),('2016-03-24 18:00:00',4.31,-7.37,1008.63,87),('2016-03-24 21:00:00',4.46,-8.73,1009.21,82),('2016-03-25 00:00:00',4.36,-7.37,1010.16,87),('2016-03-25 03:00:00',4.66,-3.62,1010.31,90),('2016-03-25 06:00:00',4.76,-2.08,1010.01,89),('2016-03-25 09:00:00',4.12,-3.72,1010.1,84),('2016-03-25 12:00:00',3.81,-6.97,1011.15,80),('2016-03-25 15:00:00',3.36,-9.08,1010.76,84),('2016-03-25 18:00:00',3.19,-10.63,1010.5,77),('2016-03-25 21:00:00',3.22,-11.56,1010.75,77),('2016-03-26 00:00:00',3.22,-8.56,1011.08,86),('2016-03-26 03:00:00',3.49,-3.62,1010.66,93),('2016-03-26 06:00:00',3.65,-1.71,1010.13,87),('2016-03-26 09:00:00',3.41,-3.52,1010.23,82),('2016-03-26 12:00:00',3.12,-8.3,1011.08,79),('2016-03-26 15:00:00',2.46,-11.2,1011.13,75),('2016-03-26 18:00:00',2.26,-12.09,1011.15,80);
/*!40000 ALTER TABLE `forecast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `powerproduction`
--

DROP TABLE IF EXISTS `powerproduction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `powerproduction` (
  `id` int(10) unsigned NOT NULL,
  `power` double unsigned NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  CONSTRAINT `FK_WINDTURBIN` FOREIGN KEY (`id`) REFERENCES `windturbins` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `powerproduction`
--

LOCK TABLES `powerproduction` WRITE;
/*!40000 ALTER TABLE `powerproduction` DISABLE KEYS */;
/*!40000 ALTER TABLE `powerproduction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producers`
--

DROP TABLE IF EXISTS `producers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producers` (
  `name` varchar(45) NOT NULL,
  `diameter` double unsigned NOT NULL,
  `blades` int(10) unsigned NOT NULL,
  `cutIn` double unsigned NOT NULL,
  `cutOut` double unsigned NOT NULL,
  `cutNom` double unsigned NOT NULL,
  `powerNom` int(10) unsigned NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producers`
--

LOCK TABLES `producers` WRITE;
/*!40000 ALTER TABLE `producers` DISABLE KEYS */;
INSERT INTO `producers` VALUES ('vestas',82,3,3.5,20,13,1650);
/*!40000 ALTER TABLE `producers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `windturbins`
--

DROP TABLE IF EXISTS `windturbins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `windturbins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `producer` varchar(45) NOT NULL,
  `longtitude` double unsigned NOT NULL,
  `latitude` double unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `FK_PRODUCER_idx` (`producer`),
  CONSTRAINT `FK_PRODUCER` FOREIGN KEY (`producer`) REFERENCES `producers` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `windturbins`
--

LOCK TABLES `windturbins` WRITE;
/*!40000 ALTER TABLE `windturbins` DISABLE KEYS */;
INSERT INTO `windturbins` VALUES (1,'vestas',37,55),(2,'vestas',38,56);
/*!40000 ALTER TABLE `windturbins` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-24 21:33:57
