package package_evtukhova;

import package_evtukhova.database.DatabaseWriter;

/**
 * Created by Екатерина on 06.03.2016.
 */
class Main {
    public static void main(String[] args) throws Exception {
        DatabaseWriter databaseWriter = new DatabaseWriter();
        databaseWriter.writeToDatabase();
    }
}
