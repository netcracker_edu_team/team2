package package_evtukhova.utils;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.interpolation.UnivariateInterpolator;

/**
 * Created by dyakin
 */
class Mathematic {

    public static UnivariateFunction interpolate(double[] y, double[] x) {
        UnivariateInterpolator interpolator = new SplineInterpolator();
        return interpolator.interpolate(x, y);
    }

   

}