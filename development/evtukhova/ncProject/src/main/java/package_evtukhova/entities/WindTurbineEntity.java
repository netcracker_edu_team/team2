package package_evtukhova.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Екатерина on 17.03.2016.
 */
@XmlRootElement
public class WindTurbineEntity  implements BaseEntity{
    @XmlElement
    private int id;
    @XmlElement
    private String producer;
    @XmlElement
    private double longtitude;
    @XmlElement
    private double latitude;
    @XmlElement
    private double cutIn;
    @XmlElement
    private double cutOut;
    @XmlElement
    private double cutNom;
    @XmlElement
    private int powerNom;

    public WindTurbineEntity() { }

    public WindTurbineEntity(int id, String producer, double longtitude, double latitude,
            double cutIn, double cutOut, double cutNom, int powerNom) {
        this.id = id;
        this.producer = producer;
        this.longtitude = longtitude;
        this.latitude = latitude;
        this.cutIn = cutIn;
        this.cutOut = cutOut;
        this.cutNom = cutNom;
        this.powerNom = powerNom;
    }

    public double getCutIn() {
        return cutIn;
    }

    public double getCutOut() {
        return cutOut;
    }

    public double getCutNom() {
        return cutNom;
    }

    public int getPowerNom() {
        return powerNom;
    }

}
