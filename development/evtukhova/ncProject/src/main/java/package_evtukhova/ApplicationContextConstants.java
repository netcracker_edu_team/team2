package package_evtukhova;

/**
 * Created by Екатерина on 20.03.2016.
 */
class ApplicationContextConstants {

    public static final String CONFIG_FILE_NAME = "config.properties";
    public static final String DB_CONFIG_FILE_NAME = "database.properties";

}
