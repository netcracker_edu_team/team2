package package_evtukhova.response_parser_api;

import package_evtukhova.entities.WeatherCondition;

import java.util.Set;

/**
 * Created by Екатерина on 11.03.2016.
 */
interface ResponseParse {
    WeatherCondition getWheatherByTime(Long time);
    Set<Long> getTimes();

}
