package package_evtukhova.response_parser_api;

import org.apache.log4j.Logger;
import package_evtukhova.entities.WeatherCondition;

import java.util.Set;

/**
 * Created by Екатерина on 12.03.2016.
 */
public class ResponseParseApiCreator implements ResponseParse {
    private static ResponseParse responseParseApi;
    private static final Logger LOG=Logger.getLogger(ResponseParseApiCreator.class);

    public ResponseParseApiCreator(String responseParserApiName, String response) {
        if (responseParserApiName.equals("xml")) {
            responseParseApi = new ResponseParseApiXML(response);
            if (LOG.isInfoEnabled())
                LOG.info("Create ResponseParseApiCreator for XML");
        } else if (responseParserApiName.equals("json")) {
            responseParseApi = new ResponseParseApiJSON(response);
            if (LOG.isInfoEnabled())
                LOG.info("Create ResponseParseApiCreator for JSON");
        }
    }

    public WeatherCondition getWheatherByTime(Long time) {
        return responseParseApi.getWheatherByTime(time);
    }

    public Set<Long> getTimes() {
        return responseParseApi.getTimes();
    }
}
