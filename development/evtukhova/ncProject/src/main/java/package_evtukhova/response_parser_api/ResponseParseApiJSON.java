package package_evtukhova.response_parser_api;

import org.apache.log4j.Logger;
import package_evtukhova.entities.WeatherCondition;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 * Created by Екатерина on 11.03.2016.
 */
class ResponseParseApiJSON extends ResponseParseApi implements ResponseParse {

    private static final Logger LOG=Logger.getLogger(ResponseParseApiJSON.class);
    public ResponseParseApiJSON(String response) {
        super("json", response);
        try {
        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date;

        int length = parser.getCountOfElements(response, "list");

        for (Integer i = 0; i < length; i++) {
            String request = "list/" + i.toString() + "/";

            String time = parser.getElementByName(response, request + "dt").toString();
            date = new Date(Long.parseLong(time) *1000);

            WeatherCondition wheather = new WeatherCondition();
            wheather.setDate(date);
            wheather.setSpeed(parser.getElementByName(response, request + "wind/speed").toString());
            wheather.setHumidity(parser.getElementByName(response, request + "main/humidity").toString());
            wheather.setPressure(parser.getElementByName(response, request + "main/pressure").toString());
            wheather.setTemperature(parser.getElementByName(response, request + "main/temp").toString());

            properties.put(Long.parseLong(time)*1000, wheather);
        }
        } catch (Exception ex) {
            LOG.error("Unable to parse date :", ex);
        }
    }


    public WeatherCondition getWheatherByTime(Long time) {
        return properties.get(time);
    }

    public Set<Long> getTimes() {
        return properties.keySet();
    }
}
