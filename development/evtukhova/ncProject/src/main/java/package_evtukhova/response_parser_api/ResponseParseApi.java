package package_evtukhova.response_parser_api;

import org.apache.log4j.Logger;
import package_evtukhova.entities.WeatherCondition;
import package_evtukhova.response_parser.ResponseParserCreator;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Екатерина on 11.03.2016.
 */
abstract class ResponseParseApi {

    final Map<Long, WeatherCondition> properties = new LinkedHashMap<Long, WeatherCondition>();
    ResponseParserCreator parser;

    ResponseParseApi() {

    }

    ResponseParseApi(String mode, String response) {
        parser = new ResponseParserCreator(mode);
        String response1 = response;
    }

}
