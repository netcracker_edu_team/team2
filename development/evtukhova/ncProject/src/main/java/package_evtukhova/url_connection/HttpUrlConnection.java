package package_evtukhova.url_connection;

/**
 * Created by Екатерина on 07.03.2016.
 */
interface HttpUrlConnection {
    String USER_AGENT = "Mozzila/5.0";

    String sendGet() throws Exception;

    String sendPost();

}
