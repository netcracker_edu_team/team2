package package_evtukhova.url_connection;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import package_evtukhova.ApplicationContext;

/**
 * Created by Екатерина on 06.03.2016.
 */
public class HttpUrlConnectionApi implements HttpUrlConnection {
    private static final Logger LOG=Logger.getLogger(HttpUrlConnectionApi.class);

    public String sendGet() throws Exception {
        String url = ApplicationContext.getPropertyByName("url") + "?q=" +
                ApplicationContext.getPropertyByName("q") + "&APPID=" +
                ApplicationContext.getPropertyByName("APPID") + "&mode=" +
                ApplicationContext.getPropertyByName("mode") + "&units=" +
                ApplicationContext.getPropertyByName("units");

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);

        request.addHeader("User-Agent", USER_AGENT);

        HttpResponse response = client.execute(request);

        if (LOG.isInfoEnabled()) {
            LOG.info("Sending 'GET' request to URL : " + url);
            LOG.info("Response Code : " + response.getStatusLine().getStatusCode());
        }

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        return result.toString();
    }

    public String sendPost() {
        if (LOG.isInfoEnabled())
            LOG.info("Sending 'POST' request to URL");
        return null;
    }
}
