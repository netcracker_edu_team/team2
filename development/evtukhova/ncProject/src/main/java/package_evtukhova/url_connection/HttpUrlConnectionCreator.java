package package_evtukhova.url_connection;

import org.apache.log4j.Logger;

/**
 * Created by Екатерина on 07.03.2016.
 */
public class HttpUrlConnectionCreator implements HttpUrlConnection{
    public enum HttpUrlConnectionName { APACHE_HTTP_CLIENT }

    private static HttpUrlConnection httpUrlConnection;
    private static final Logger LOG=Logger.getLogger(HttpUrlConnectionCreator.class);

    public HttpUrlConnectionCreator(HttpUrlConnectionName httpUrlConnectionName) {
        if (httpUrlConnectionName == HttpUrlConnectionName.APACHE_HTTP_CLIENT) {
            httpUrlConnection = new HttpUrlConnectionApi();
            if (LOG.isInfoEnabled())
                LOG.info("Create connection with Apache Http Client");
        }
    }

    public String sendGet(){
        try {
            return httpUrlConnection.sendGet();
        } catch (Exception ex) {
            LOG.error("Enable execute GET request", ex);
        }
        return null;
    }

    public String sendPost() {
        try {
            return httpUrlConnection.sendPost();
        } catch (Exception ex) {
            LOG.error("Enable execute POST request", ex);
        }
        return null;
    }

}
