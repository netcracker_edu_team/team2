package package_evtukhova;

import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ApplicationContext {
    private static ApplicationContext instance;
    private static final Map<String, String> properties = new HashMap<String, String>();
    private static InputStream inputStream;
    private static final Logger LOG=Logger.getLogger(ApplicationContext.class);

    private void readPropertiesConfigFile(Properties prop) {
        properties.put("url", prop.getProperty("url"));
        properties.put("id", prop.getProperty("id"));
        properties.put("q", prop.getProperty("q"));
        properties.put("units", prop.getProperty("units"));
        properties.put("APPID", prop.getProperty("APPID"));
        properties.put("mode", prop.getProperty("mode"));
    }

    private void readPropertiesDatabaseConfigFile(Properties prop) {
        properties.put("DB_URL", prop.getProperty("DB_URL"));
        properties.put("DB_USERNAME", prop.getProperty("DB_USERNAME"));
        properties.put("DB_PASSWORD", prop.getProperty("DB_PASSWORD"));
    }


    private void readFromConfigFile(String fileName) throws IOException {
        try {
            Properties prop = new Properties();

            inputStream = getClass().getClassLoader().getResourceAsStream(fileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                LOG.error("Property file not found in the classpath, file name: " + fileName);
                throw new FileNotFoundException("Property file '" + fileName + "' not found in the classpath");
            }

            if (fileName.equals(ApplicationContextConstants.CONFIG_FILE_NAME)) {
                readPropertiesConfigFile(prop);
            } else if (fileName.equals(ApplicationContextConstants.DB_CONFIG_FILE_NAME)) {
                readPropertiesDatabaseConfigFile(prop);
            }

        } catch (Exception e) {
            LOG.error("Property file not found in the classpath" , e);
        } finally {
            inputStream.close();
        }
    }

    private ApplicationContext() {
        try {
            readFromConfigFile(ApplicationContextConstants.CONFIG_FILE_NAME);
            readFromConfigFile(ApplicationContextConstants.DB_CONFIG_FILE_NAME);
        } catch (Exception e) {
            LOG.error("Property file not found in the classpath" , e);
        }
    }

    public static synchronized String getPropertyByName(String name) {
        if (instance == null) {
            instance = new ApplicationContext();
            LOG.info("Create ApplicationContext");
        }
        return properties.get(name);

    }
}