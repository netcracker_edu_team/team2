package package_evtukhova.database;

import package_evtukhova.ApplicationContext;
import package_evtukhova.entities.PowerEntity;
import package_evtukhova.entities.WeatherCondition;
import package_evtukhova.entities.WindTurbineEntity;
import package_evtukhova.response_parser_api.ResponseParseApiCreator;
import package_evtukhova.url_connection.HttpUrlConnectionCreator;

import java.util.Collection;

/**
 * Created by Екатерина on 12.03.2016.
 */
public class DatabaseWriter {
    private final HttpUrlConnectionCreator httpConnection=
            new HttpUrlConnectionCreator(HttpUrlConnectionCreator.HttpUrlConnectionName.APACHE_HTTP_CLIENT);
    private final DatabaseConnection databaseConnection = new DatabaseConnectionImpl();

    public void writeToDatabase() {
        String response = httpConnection.sendGet();
        ResponseParseApiCreator responseParseApiCreator = new ResponseParseApiCreator(ApplicationContext.getPropertyByName("mode"),
                response);
        //databaseConnection.truncateForecast();
        databaseConnection.insertForecast(responseParseApiCreator);
    }

    public WindTurbineEntity getWindTurbineById(int id) {
        return databaseConnection.getWindTurbineById(id);
    }

    public Collection<WindTurbineEntity> getWindTurbines() {
        return databaseConnection.getWindTurbines();
    }

    public Collection<PowerEntity> getConsumersConsumption() {
        return databaseConnection.getConsumersConsumption();
    }

    public Collection<WeatherCondition> getForecast() {
        return databaseConnection.getWeather();
    }
}
