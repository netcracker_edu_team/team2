package package_evtukhova.database;

import package_evtukhova.entities.PowerEntity;
import package_evtukhova.entities.WeatherCondition;
import package_evtukhova.entities.WindTurbineEntity;
import package_evtukhova.response_parser_api.ResponseParseApiCreator;

import java.sql.Connection;
import java.util.Collection;

/**
 * Created by Екатерина on 16.03.2016.
 */
interface DatabaseConnection {
    Connection getConnection();
    void closeConnection();
    void insertForecast(ResponseParseApiCreator responseParseApiCreator);
    void truncateForecast();

    WindTurbineEntity getWindTurbineById(int id);
    Collection<WindTurbineEntity> getWindTurbines();
    Collection<PowerEntity> getConsumersConsumption();
    Collection<WeatherCondition> getWeather();
}
