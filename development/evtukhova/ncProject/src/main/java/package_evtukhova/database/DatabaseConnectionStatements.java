package package_evtukhova.database;


/**
 * Created by Екатерина on 20.03.2016.
 */
class DatabaseConnectionStatements {
    public static final String INSERT_FORECAST
            = "INSERT INTO forecast(time, windSpeed, temperature, pressure, humidity) " +
            "VALUES ( FROM_UNIXTIME(?/1000), ?, ?, ?, ?)" +
            "ON DUPLICATE KEY UPDATE windSpeed = ?, temperature = ?, pressure = ?, humidity = ?";

    public static final String SELECT_TURBINE_BY_ID = "SELECT id, producer, longtitude, latitude," +
            "cutIn, cutOut, cutNom, powerNom FROM windturbins JOIN producers \n" +
            "ON windturbins.producer=producers.name \n" +
            "WHERE id=?";

    public static final String SELECT_TURBINES = "SELECT id, producer, longtitude, latitude," +
            "cutIn, cutOut, cutNom, powerNom FROM windturbins JOIN producers \n" +
            "ON windturbins.producer=producers.name";

    public static final String SELECT_CONSUMERS_CONSUMPTION = "SELECT time, power FROM consumersconsumption;";

    public static final String SELECT_FORECAST = "SELECT * FROM forecast;";
}
