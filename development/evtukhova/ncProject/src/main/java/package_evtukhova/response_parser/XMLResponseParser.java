package package_evtukhova.response_parser;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by Екатерина on 06.03.2016.
 */

public class XMLResponseParser implements ResponseParser {
    private static final DocumentBuilderFactory dbFactory
            = DocumentBuilderFactory.newInstance();
    private static final Logger LOG=Logger.getLogger(XMLResponseParser.class);

    private static String nodeToString(Node node) throws TransformerException
    {
        StringWriter buf = new StringWriter();
        Transformer xform = TransformerFactory.newInstance().newTransformer();
        xform.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        xform.transform(new DOMSource(node), new StreamResult(buf));
        return(buf.toString());
    }

    public Object getElementByName(String input, String element) {
        try {
            Document doc = dbFactory.newDocumentBuilder().parse(new InputSource(new StringReader(input)));

            XPath xPath = XPathFactory.newInstance().newXPath();
            Node result = (Node)xPath.evaluate(element, doc, XPathConstants.NODE);

            if (result.getNodeValue() == null)
                return nodeToString(result);
            return result.getNodeValue();
        } catch (Exception e) {
            LOG.error("Error parse XML by name:", e);
        }

        return null;
    }

    public int getCountOfElements(String input, String element) {
        try {
            Document doc = dbFactory.newDocumentBuilder().parse(new InputSource(new StringReader(input)));

            //XPath xPath = XPathFactory.newInstance().newXPath();
            //NodeList result = (NodeList)xPath.evaluate(element, doc, XPathConstants.NODE);
            NodeList result = doc.getElementsByTagName(element);
            return result.getLength();
        } catch (Exception e) {
            LOG.error("Error parse XML by name:", e);
        }

        return 0;
    }

}
