package package_evtukhova.response_parser;

/**
 * Created by Екатерина on 06.03.2016.
 */
interface ResponseParser {

    Object getElementByName(String input, String element);
    int getCountOfElements(String input, String element);
}
