package com.service;

import com.response.JSONWeatherCatcher;
import com.response.XMLWeatherCatcher;

public class WeatherFactory {
    public static WeatherParser getParser(String type) {
        WeatherParser weatherCatcher = null;

        if (type.equals("xml")) // something... switch(prop.type)
            weatherCatcher = new XMLWeatherCatcher(type);
        else if (type.equals("json"))
            weatherCatcher = new JSONWeatherCatcher(type);

        return weatherCatcher;
    }
}
