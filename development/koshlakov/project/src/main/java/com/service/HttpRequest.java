package com.service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpRequest {

    private static final String USER_AGENT = "Mozilla/5.0";

    public static String sendPost(String url) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection)obj.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-Us,en;q=0.5");

        int responseCode = con.getResponseCode();
        System.out.println("Sending `POST` request to URL: " + url);
        System.out.println("service Code: " + responseCode);

        if (responseCode == 200) {
            BufferedReader bufReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String readLine;
            StringBuffer respone = new StringBuffer();

            while ((readLine = bufReader.readLine()) != null)
                respone.append(readLine + "\n");

            bufReader.close();

            return respone.toString();
        }

        return null;
    }
}
