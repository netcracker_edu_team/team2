package com.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ApplicationContext {
    private static ApplicationContext instance;
    private static Map<String, String> properties = new HashMap<String, String>();


    public static String getPropertyByName(String name) {
        if (instance == null)
            instance = new ApplicationContext();

        return properties.get(name);
    }

    private ApplicationContext() {
        try {
            String configFileName = "config.properties";
            InputStream inputStream = getClass().getClassLoader().
                    getResourceAsStream(configFileName);

            Properties prop = new Properties();

            if (inputStream != null)
                prop.load(inputStream);
            else
                throw new FileNotFoundException(configFileName + " not found in project directory");

            properties.put("url", prop.getProperty("url"));
            properties.put("q", prop.getProperty("q"));
            properties.put("units", prop.getProperty("units"));
            properties.put("appid", prop.getProperty("appid"));
            properties.put("mode", prop.getProperty("mode"));

            properties.put("db_url", prop.getProperty("db_url"));
            properties.put("db_user", prop.getProperty("db_user"));
            properties.put("db_pas", prop.getProperty("db_pas"));

            inputStream.close();
        }catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
