package com.service;

import com.model.WeatherEntity;

import java.util.Collection;

public abstract class WeatherParser {

    public Collection<WeatherEntity> order(String type, String url) throws Exception {
        Collection<WeatherEntity> list;

        list = getWeather(type, url);

        return list;
    }

    protected abstract Collection<WeatherEntity> getWeather(String type, String url) throws Exception;
}
