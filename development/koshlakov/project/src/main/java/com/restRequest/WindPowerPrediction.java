package com.restRequest;

import com.dataBase.DBWrite;
import com.model.PowerEntity;
import com.model.WeatherEntity;
import com.model.WindTurbineEntity;
import com.util.PowerCalculation;

import java.util.Collection;

/**
 * Created by НИКИТА on 28.03.2016.
 */
public class WindPowerPrediction {
    public static Collection<PowerEntity> getWindPowerPrediction(DBWrite dbWrite) {
        Collection<WindTurbineEntity> windTurbineEntities = dbWrite.getWindTurbines();
        Collection<WeatherEntity> weatherEntities = dbWrite.getForecast();
        PowerCalculation powerCalculation
                = new PowerCalculation((WindTurbineEntity)(windTurbineEntities.toArray())[0]);

        Collection<PowerEntity> list = powerCalculation.estimatePower(weatherEntities);
        for (int i = 1; i < windTurbineEntities.size(); i++) {
            powerCalculation = new PowerCalculation((WindTurbineEntity)(windTurbineEntities.toArray())[i]);
            Collection<PowerEntity> tmp = powerCalculation.estimatePower(weatherEntities);
            for (int j = 0; j < tmp.size(); j++) {
                ((PowerEntity)(list.toArray()[j])).setPower(
                        ((PowerEntity)(list.toArray()[j])).getPower()
                                +
                                ((PowerEntity)(tmp.toArray()[j])).getPower()
                );
            }
        }

        return list;
    }
}
