package com.restRequest;

import com.dataBase.DBWrite;
import com.model.WeatherEntity;
import com.util.PowerCalculation;
import com.model.WindTurbineEntity;
import com.model.PowerEntity;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Path("/service")
public class WebService {
    private static final Logger LOG = Logger.getLogger(WebService.class);

    @GET
    @Path("/{message}")
    @Produces("text/plain")
    public String getMsg(@PathParam("message") String msg) {
        String output = "You say: " + msg;

        return output;
    }

    @GET
    @Path("/getTurbines")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<WindTurbineEntity> getWindTurbines() {
        try {
            DBWrite dbWrite = new DBWrite();
            if (LOG.isInfoEnabled())
                LOG.info("User request WindTurbines");
            return dbWrite.getWindTurbines();
        }catch (Exception e) {
            LOG.error("Enable execute request: getWindTurbines", e);
            throw new WebApplicationException(404);
        }
    }

    @GET
    @Path("/getWindTurbineById")
    @Produces(MediaType.APPLICATION_JSON)
    public WindTurbineEntity getWindTurbineById(@QueryParam("id") int id) {
        try {
            DBWrite dbWrite = new DBWrite();
            if (LOG.isInfoEnabled())
                LOG.info("User was request getWindTurbine by id = " + id);
            return dbWrite.getWindTurbineById(id);
        }catch (Exception e) {
            LOG.error("Enable execute request: getWindTurbines by id = " + id, e);
            throw new WebApplicationException(404);
        }
    }

    @GET
    @Path("/getConsumersConsumption")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<PowerEntity> getConsumersConsumption() {
        try {
            DBWrite dbWrite = new DBWrite();
            if (LOG.isInfoEnabled())
                LOG.info("User was request getConsumersConsumption");
            return dbWrite.getConsumersConsumption();
        }catch (Exception e) {
            LOG.error("Enable execute request: getConsumersConsumption", e);
            throw new WebApplicationException(404);
        }
    }

    @GET
    @Path("/getWindPowerPredictionById")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<PowerEntity> getWindPowerPredictionById(@QueryParam("id") int id) {
        try {
            DBWrite dbWrite = new DBWrite();
            dbWrite.writeWeather();
            PowerCalculation powerCalculation = new PowerCalculation(dbWrite.getWindTurbineById(id));
            if (LOG.isInfoEnabled())
                LOG.info("User was request getWindPowerPrediction by id = " + id);
            return powerCalculation.estimatePower(dbWrite.getForecast());
        }catch (Exception e) {
            LOG.error("Enable execute request: getWindPowerPrediction by id = " + id, e);
            throw new WebApplicationException(404);
        }
    }

    @GET
    @Path("/getWindPowerPrediction")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<PowerEntity> getWindPowerPrediction() {
        try {
            DBWrite dbWrite = new DBWrite();
            dbWrite.writeWeather();
            if (LOG.isInfoEnabled())
                LOG.info("User was request getWindPowerPrediction");
            return WindPowerPrediction.getWindPowerPrediction(dbWrite);
        }catch (Exception e) {
            LOG.error("Enable execute request: getWindPowerPrediction", e);
            throw new WebApplicationException(404);
        }
    }
}
