package com.model;

import java.util.Date;

public class WeatherEntity {
    private Double temperature;
    private Double pressure;
    private Integer humidity;
    private Double wind;
    private Date date;

    public WeatherEntity() {
    }

    public WeatherEntity(double Temperature, double Pressure,
                         int Humidity, double Wind, java.sql.Date date) {
        temperature = Temperature;
        pressure = Pressure;
        humidity = Humidity;
        wind = Wind;
        this.date = new Date(date.getTime());
    }

    public double getTemperature() { return temperature; }
    public void setTemperature(double temperature) { this.temperature = temperature; }

    public double getPressure() { return pressure; }
    public void setPressure(double pressure) { this.pressure = pressure; }

    public int getHumidity() { return humidity; }
    public void setHumidity(int humidity) { this.humidity = humidity; }

    public double getWind() { return wind; }
    public void setWind(double Wind) { this.wind = Wind; }

    public Date getDate() { return date; }
    public void setDate(Date date) { this.date = date; }

    @Override
    public String toString() {
        return "WeatherEntity{" +
                "temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", wind=" + wind +
                ", date=" + date +
                '}';
    }
}