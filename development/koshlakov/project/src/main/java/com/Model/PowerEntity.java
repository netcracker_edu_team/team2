package com.model;

import java.util.Date;

public class PowerEntity {
    private Integer power;
    private Date date;
    private Long id;
    private Integer version;
    private Double wind;
    private Double pressure;
    private Double temperature;

    public PowerEntity() {
    }

    public PowerEntity(Integer power, Date date, Long id,
                       Integer version, Double wind,
                       Double pressure, Double temperature) {
        this.power = power;
        this.date = date;
        this.id = id;
        this.version = version;
        this.wind = wind;
        this.pressure = pressure;
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "PowerEntity{" +
                "Power=" + power +
                ", date=" + date +
                ", id=" + id +
                ", version=" + version +
                ", wind=" + wind +
                ", pressure=" + pressure +
                ", temperature=" + temperature +
                '}';
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Double getWind() {
        return wind;
    }

    public void setWind(Double wind) {
        this.wind = wind;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }
}
