package com.model;

public class WindTurbineEntity {
    private Integer id;
    private String producer;
    private Double longtitude;
    private Double latitude;
    private Double cutIn;
    private Double cutOut;
    private Double cutNom;
    private Integer powerNom;

    public WindTurbineEntity() {
    }

    public WindTurbineEntity(Integer id, String producer, Double longtitude, Double latitude, Double cutIn, Double cutOut, Double cutNom, Integer powerNom) {
        this.id = id;
        this.producer = producer;
        this.longtitude = longtitude;
        this.latitude = latitude;
        this.cutIn = cutIn;
        this.cutOut = cutOut;
        this.cutNom = cutNom;
        this.powerNom = powerNom;
    }

    @Override
    public String toString() {
        return "WindTurbineEntity{" +
                "id=" + id +
                ", producer='" + producer + '\'' +
                ", longtitude=" + longtitude +
                ", latitude=" + latitude +
                ", cutIn=" + cutIn +
                ", cutOut=" + cutOut +
                ", cutNom=" + cutNom +
                ", powerNom=" + powerNom +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public Double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(Double longtitude) {
        this.longtitude = longtitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getCutIn() {
        return cutIn;
    }

    public void setCutIn(Double cutIn) {
        this.cutIn = cutIn;
    }

    public Double getCutOut() {
        return cutOut;
    }

    public void setCutOut(Double cutOut) {
        this.cutOut = cutOut;
    }

    public Double getCutNom() {
        return cutNom;
    }

    public void setCutNom(Double cutNom) {
        this.cutNom = cutNom;
    }

    public Integer getPowerNom() {
        return powerNom;
    }

    public void setPowerNom(Integer powerNom) {
        this.powerNom = powerNom;
    }
}
