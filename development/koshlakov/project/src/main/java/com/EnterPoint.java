package com;

import com.dataBase.DBWrite;
import com.model.WeatherEntity;

import com.service.ApplicationContext;
import com.service.WeatherFactory;
import com.service.WeatherParser;

import java.util.Collection;
import java.util.LinkedList;

public class EnterPoint {

    public static void main(String... arg) throws Exception {
        DBWrite dbWrite = new DBWrite();
        dbWrite.writeWeather();
        System.out.println(dbWrite.getWindTurbineById(1));
    }
}
