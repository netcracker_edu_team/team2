package com.dataBase;

import com.model.PowerEntity;
import com.model.WeatherEntity;
import com.model.WindTurbineEntity;
import com.service.ApplicationContext;
import com.service.WeatherFactory;
import com.service.WeatherParser;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by НИКИТА on 25.03.2016.
 */
public class DBWrite {
    private final DataBase dataBase = new DataBaseImpl();

    public DataBase getDataBase() { return dataBase; }

    public void writeWeather() {
        try {
            String type = ApplicationContext.getPropertyByName("mode");

            String request = ApplicationContext.getPropertyByName("url")
                    + "?q=" + ApplicationContext.getPropertyByName("q")
                    + "&appid=" + ApplicationContext.getPropertyByName("appid")
                    + "&mode=" + ApplicationContext.getPropertyByName("mode")
                    + "&units=" + ApplicationContext.getPropertyByName("units");

            Collection<WeatherEntity> list;
            WeatherParser weatherParser = WeatherFactory.getParser(type);
            list = weatherParser.order(type, request);

            dataBase.insertWeather(list);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public WindTurbineEntity getWindTurbineById(int id) {
        return dataBase.getWindTurbineById(id);
    }

    public Collection<WindTurbineEntity> getWindTurbines() {
        return dataBase.getWindTurbines();
    }

    public Collection<PowerEntity> getConsumersConsumption() {
        return dataBase.getConsumersConsumption();
    }

    public Collection<WeatherEntity> getForecast() {
        return dataBase.getWeather();
    }
}
