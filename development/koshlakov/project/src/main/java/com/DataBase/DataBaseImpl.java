package com.dataBase;

import com.model.PowerEntity;
import com.model.WeatherEntity;
import com.model.WindTurbineEntity;
import com.mysql.fabric.jdbc.FabricMySQLDriver;
import com.service.ApplicationContext;
import org.apache.log4j.Logger;

import java.sql.*;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class DataBaseImpl implements DataBase {
    private Connection connection;
    private static final Logger LOG = Logger.getLogger(DataBaseImpl.class);

    public DataBaseImpl() {
        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            if (LOG.isInfoEnabled())
                LOG.info("Drive register");

            getConnection();
            closeConnection();
        }catch (SQLException e) {
            LOG.error("Could`t connect to database ", e);
        }
    }

    public Connection getConnection() {
        try {
            connection = DriverManager.getConnection(
                    ApplicationContext.getPropertyByName("db_url"),
                    ApplicationContext.getPropertyByName("db_user"),
                    ApplicationContext.getPropertyByName("db_pas"));

            if (connection.isClosed())
                throw new SQLException();

            if (LOG.isInfoEnabled())
                LOG.info("Connect successfully");
        }catch (SQLException e) {
            LOG.error("Could`t not connect to database", e);
        }finally {
            return connection;
        }
    }

    public void closeConnection() {
        try {
            connection.close();
            if (LOG.isInfoEnabled())
                LOG.info("Connect close successfully");
        } catch (SQLException e) {
            LOG.error("Could`t not close connection to database", e);
        }
    }

    public void insertWeather(Collection<WeatherEntity> weatherEntety) {
        try {
            getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(DBStatement.INSERT_WEATHER);

            for (WeatherEntity w : weatherEntety) {
                preparedStatement.setDouble(1, w.getTemperature());
                preparedStatement.setDouble(2, w.getPressure());
                preparedStatement.setInt(3, w.getHumidity());
                preparedStatement.setDouble(4, w.getWind());
                preparedStatement.setDate(5, new Date(w.getDate().getTime()));

                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
            connection.commit();

            if (LOG.isInfoEnabled())
                LOG.info("Weather was insered to database");
        } catch (SQLException e) {
            LOG.error("Enable execute statement", e);
            try {
                connection.rollback();
            } catch (SQLException ex) {
                LOG.error("Enable rollback transaction", ex);
            }
        } finally {
            closeConnection();
        }
    }

    public void cleanTableWeather() {
        try {
            getConnection();
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM tb_date");

            if (LOG.isInfoEnabled())
                LOG.info("Table weather was truncated");
        }catch (SQLException e) {
            LOG.error("Enable execute statement", e);
        }finally {
            closeConnection();
        }
    }

    public WindTurbineEntity getWindTurbineById(int id) {
        try {
            getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DBStatement.SELECT_TURBINE_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return new WindTurbineEntity(resultSet.getInt("id"), resultSet.getString("producer"),
                        resultSet.getDouble("longtitude"), resultSet.getDouble("latitude"),
                        resultSet.getDouble("cutIn"),resultSet.getDouble("cutOut"),
                        resultSet.getDouble("cutNom"), resultSet.getInt("powerNom"));
            }

            if (LOG.isInfoEnabled())
                LOG.info("Turbine with id = " + id + " was selected");
        }catch (SQLException e) {
            LOG.error("Enable execute statement", e);
        }finally {
            closeConnection();
        }
        return null;
    }

    public Collection<WindTurbineEntity> getWindTurbines() {
        try {
            getConnection();
            ResultSet resultSet = connection.createStatement().executeQuery(DBStatement.SELECT_TURBINES);

            Collection<WindTurbineEntity> array = new ArrayList<WindTurbineEntity>();
            while (resultSet.next()) {
                array.add(new WindTurbineEntity(resultSet.getInt("id"), resultSet.getString("producer"),
                        resultSet.getDouble("longtitude"), resultSet.getDouble("latitude"),
                        resultSet.getDouble("cutIn"),resultSet.getDouble("cutOut"),
                        resultSet.getDouble("cutNom"), resultSet.getInt("powerNom")));
            }

            if (LOG.isInfoEnabled())
                LOG.info("Turbine was selected");

            return array;
        }catch (SQLException e){
            LOG.error("Enable execute statement", e);
        }finally {
            closeConnection();
        }
        return null;
    }

    public Collection<PowerEntity> getConsumersConsumption() {
        try {
            getConnection();
            ResultSet resultSet = connection.createStatement().executeQuery(DBStatement.SELECT_CONSUMERS_CONSUMPTION);

            Collection<PowerEntity> list = new ArrayList<PowerEntity>();
            while (resultSet.next()) {
                PowerEntity powerEntity = new PowerEntity();
                powerEntity.setDate(resultSet.getDate("time"));
                powerEntity.setPower((int)resultSet.getDouble("power"));
                list.add(powerEntity);
            }

            if (LOG.isInfoEnabled())
                LOG.info("Consumers consumption was selected");

            return list;
        }catch (SQLException e) {
            LOG.error("Enable execute statement", e);
        }finally {
            closeConnection();
        }
        return null;
    }

    public Collection<WeatherEntity> getWeather() {
        try {
            getConnection();
            ResultSet resultSet = connection.createStatement().executeQuery(DBStatement.SELECT_WEATHER);

            Collection<WeatherEntity> list = new ArrayList<WeatherEntity>();
            while (resultSet.next()) {
                list.add(new WeatherEntity(resultSet.getDouble("degree"), resultSet.getDouble("pressure"),
                        resultSet.getInt("humidity"), resultSet.getDouble("speed_wind"),
                        resultSet.getDate("time_from"))
                );
            }

            if (LOG.isInfoEnabled())
                LOG.info("Weather was selected");

            return list;
        }catch (SQLException e) {
            LOG.error("Enable execute statement", e);
        }finally {
            closeConnection();
        }
        return null;
    }
}
