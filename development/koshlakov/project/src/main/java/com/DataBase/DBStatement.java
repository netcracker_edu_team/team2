package com.dataBase;

/**
 * Created by НИКИТА on 25.03.2016.
 */
public class DBStatement {
    public static final String INSERT_WEATHER = "INSERT INTO " +
            "tb_date(degree, pressure, humidity, speed_wind, time_from) " +
            "VALUES (?,?,?,?,?)";

    public static final String SELECT_TURBINE_BY_ID = "SELECT id, producer, longtitude, latitude," +
            "cutIn, cutOut, cutNom, powerNom FROM windturbins JOIN producers \n" +
            "ON windturbins.producer=producers.name \n" +
            "WHERE id=?";

    public static final String SELECT_TURBINES = "SELECT id, producer, longtitude, latitude," +
            "cutIn, cutOut, cutNom, powerNom FROM windturbins JOIN producers \n" +
            "ON windturbins.producer=producers.name";

    public static final String SELECT_CONSUMERS_CONSUMPTION = "SELECT time, power FROM consumersconsumption;";

    public static final String SELECT_WEATHER = "SELECT * FROM tb_date";
}
