package com.dataBase;

import com.model.PowerEntity;
import com.model.WeatherEntity;
import com.model.WindTurbineEntity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Collection;

/**
 * Created by НИКИТА on 25.03.2016.
 */
public interface DataBase {
    Connection getConnection();
    void closeConnection();

    void insertWeather(Collection<WeatherEntity> weatherEntety);
    void cleanTableWeather();

    WindTurbineEntity getWindTurbineById(int id);
    Collection<WindTurbineEntity> getWindTurbines();
    Collection<PowerEntity> getConsumersConsumption();
    Collection<WeatherEntity> getWeather();
}
