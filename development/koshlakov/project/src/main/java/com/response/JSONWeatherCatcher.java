package com.response;

import com.model.WeatherEntity;
import com.service.HttpRequest;
import com.service.WeatherParser;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.LinkedList;

public class JSONWeatherCatcher extends WeatherParser {
    private static ResponseParseCreator parser;

    public JSONWeatherCatcher(String format) {
        parser = new ResponseParseCreator(format);
    }

    public Collection<WeatherEntity> getWeather(String type, String url) throws Exception {
        return parseString(HttpRequest.sendPost(url));
    }

    private Collection<WeatherEntity> parseString(String response) throws Exception {
        Collection<WeatherEntity> list = new LinkedList<WeatherEntity>();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        for (Integer i = 0; i < 36; i++) {
            String request = "list/" + i.toString() + "/";

            java.util.Date date = dateFormat.parse(parser.getElementByName(response, request + "dt_txt").toString());
            date.setHours(date.getHours()-3);

            WeatherEntity we = new WeatherEntity();
            we.setTemperature(new Double(parser.getElementByName(response, request + "main/temp").toString()));
            we.setPressure(new Double(parser.getElementByName(response, request + "main/pressure").toString()));
            we.setHumidity(new Integer(parser.getElementByName(response, request + "main/humidity").toString()));
            we.setWind(new Double(parser.getElementByName(response, request + "wind/speed").toString()));
            we.setDate(new Date(date.getTime()));

            list.add(we);
        }

        return list;
    }
}