package com.response;

import com.model.WeatherEntity;
import com.service.HttpRequest;
import com.service.WeatherParser;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class XMLWeatherCatcher extends WeatherParser {
    private static ResponseParseCreator parser;

    public XMLWeatherCatcher(String format) {
        parser = new ResponseParseCreator(format);
    }

    public Collection<WeatherEntity> getWeather(String type, String url) throws Exception {
        return getElement(HttpRequest.sendPost(url));
    }

    private Collection<WeatherEntity> getElement(String response) throws ParseException {
        List<WeatherEntity> list = new LinkedList<WeatherEntity>();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        java.util.Date date;

        for (Integer i = 1; i < 36; i++) {
            String request = "weatherdata/forecast/time[" + i.toString() + "]/";
            String time_from = parser.getElementByName(response, request + "@from").toString();

            date = dateFormat.parse(time_from);

            WeatherEntity we = new WeatherEntity();
            we.setTemperature(new Double(parser.getElementByName(response,
                    request + "temperature/@value").toString()));
            we.setPressure(new Double(parser.getElementByName(response,
                    request + "pressure/@value").toString()));
            we.setHumidity(new Integer(parser.getElementByName(response,
                    request + "humidity/@value").toString()));
            we.setWind(new Double(parser.getElementByName(response,
                    request + "windSpeed/@mps").toString()));
            we.setDate(new Date(date.getTime()));

            list.add(we);
        }

        return list;
    }
}


