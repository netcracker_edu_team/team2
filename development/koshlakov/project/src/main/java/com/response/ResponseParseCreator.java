package com.response;

public class ResponseParseCreator implements ResponseParser {
    private static ResponseParser responseParser;

    public ResponseParseCreator(String parserName) {
        if (parserName.equals("xml"))
            responseParser = new XMLResponseParser();
        else if (parserName.equals("json"))
            responseParser = new JSONResponseParser();
    }

    public Object getElementByName(String input, String element) {
        return responseParser.getElementByName(input, element);
    }
}
