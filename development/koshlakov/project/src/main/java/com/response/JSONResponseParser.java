package com.response;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONResponseParser implements ResponseParser {
    static JSONParser parser = new JSONParser();

    private Object getElementByPartName(String input, String element) {
        try{
            JSONObject obj = (JSONObject) parser.parse(input);
            return obj.get(element);
        } catch(ParseException pe){
            pe.printStackTrace();
        }
        return null;
    }

    private boolean isNumber(String element) {
        try {
            Integer i = Integer.valueOf(element);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public Object getElementByName(String input, String element) {
        String[] elements = element.split("/");
        String tmpJson = input;

        for (String str: elements ) {
            if (isNumber(str)) {
                tmpJson = getElementById(tmpJson, Integer.valueOf(str)).toString();
            } else {
                tmpJson = getElementByPartName(tmpJson, str).toString();
            }
        }
        return tmpJson;
    }

    private Object getElementById(String input, Integer element) {
        try{
            JSONArray obj = (JSONArray) parser.parse(input);
            return obj.get(element);
        } catch(ParseException pe){
            pe.printStackTrace();
        }
        return null;
    }
}