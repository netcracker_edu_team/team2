package com.response;


public interface ResponseParser {
    Object getElementByName(String input, String element);
}