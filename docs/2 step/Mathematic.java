

/**
 * Created by dyakin
 */
public class Mathematic {

    public static UnivariateFunction interpolate(double[] y, double[] x) {
        UnivariateInterpolator interpolator = new SplineInterpolator();
        UnivariateFunction function = interpolator.interpolate(x, y);
        return function;
    }

   

}