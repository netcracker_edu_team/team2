


import java.util.Date;

public class PowerEntity {

    Integer power;
    Date date;
    @Id
    private Long id;
    private Integer version;
    private Double wind;
    private Double pressure;
    private Double temperature;


    public Double getWind() {
        return wind;
    }

    public void setWind(Double wind) {
        this.wind = wind;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "PowerEntity{" +
                "power=" + power +
                ", date=" + date +
                ", id=" + id +
                ", version=" + version +
                ", wind=" + wind +
                ", pressure=" + pressure +
                ", temperature=" + temperature +
                '}';
    }
}
