Для удобства обращения с BitBucket стоит использовать клиент SourceTree ( sourcetreeapp.com ).
Он использует в себе такую модель ветвления, как Git Flow . Подробнее про нее можно прочитать в следующих статьях:
https://habrahabr.ru/post/106912/
http://danielkummer.github.io/git-flow-cheatsheet/index.ru_RU.html

Для знакомства с командами, используемыми в Git, и его основными принципами рекомендую следующие статьи:
https://www.atlassian.com/git/tutorials/
http://git-scm.com/doc
https://try.github.io/levels/1/challenges/1